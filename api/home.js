import request from "@/utils/request.js";


// 获取系统信息
export function getSysSettings() {
	return request({
		url: `/jeecg-boot/home/page/getSetUp`,
		method: 'get',
	})
}

// 授权登录
export function getAuth(data) {
	return request({
		url: `/jeecg-boot/weChat/info`,
		method: 'post',
		data
	})
}

// code登录
export function loginByCode(data) {
	return request({
		url: `/jeecg-boot/weChat/code`,
		method: 'post',
		data
	})
}

// 获取环境
export function getSensor(data) {
	return request({
		url: `/jeecg-boot/home/page/getSensor`,
		method: 'get',
		data
	})
}

// 获取天气
export function getWeather(data) {
	return request({
		url: `/jeecg-boot/home/page/getWeather`,
		method: 'get',
	})
}

// 获取摄像头
export function getCamera(data) {
	return request({
		url: `/jeecg-boot/home/page/getCamera`,
		method: 'get',
	})
}

// 获取摄像头
export function login(data) {
	return request({
		url: `/jeecg-boot/weChat/login`,
		method: 'post',
		data
	})
}

// 景区故事
export function parkStory(data) {
	return request({
		url: `/jeecg-boot/api/park/story`,
		method: 'get',
		data
	})
}


// 发现景区
export function parkIntroduce(data) {
	return request({
		url: `/jeecg-boot/api/park/introduce`,
		method: 'get',
		data
	})
}

// 景区概况
export function survey(data) {
	return request({
		url: `/jeecg-boot/api/park/survey`,
		method: 'get',
		data
	})
}

// 景区通知
export function notice(data) {
	return request({
		url: `/jeecg-boot/api/park/notice`,
		method: 'get',
		data
	})
}

// 投诉建议
export function submit(data) {
	return request({
		url: `/jeecg-boot/api/park/complaint/submit`,
		method: 'post',
		data
	})
}

// 景区详情
export function ticketSetUp() {
	return request({
		url: `/jeecg-boot/api/park/ticketSetUp`,
		method: 'get',
	})
}

// 景区分类
export function ticketCategory() {
	return request({
		url: `/jeecg-boot/api/park/ticketCategory`,
		method: 'get',
	})
}

// 景区列表
export function ticket(data) {
	return request({
		url: `/jeecg-boot/api/park/ticket`,
		method: 'get',
		data
	})
}


// 计算订单价格
export function computeOrderPrice(data) {
	return request({
		url: `/jeecg-boot/park/parkTicketOrder/computeOrderPrice`,
		method: 'post',
		data
	})
}

// 创建订单
export function createOrder(data) {
	return request({
		url: `/jeecg-boot/park/parkTicketOrder/createOrder`,
		method: 'post',
		data
	})
}

// 景区导览
export function navigation(data) {
	return request({
		url: `/jeecg-boot/api/park/navigation`,
		method: 'get',
		data
	})
}


// 地块信息
export function landInfo(data) {
	return request({
		url: `/jeecg-boot/land/landManageRecord/list/mp`,
		method: 'get',
		data
	})
}

// 地块信息
export function landSubmit(data) {
	return request({
		url: `/jeecg-boot/land/landManageRecord/complete`,
		method: 'post',
		data
	})
}


// 地块巡检记录
export function landManageRecord(data) {
	return request({
		url: `/jeecg-boot/land/landManageRecord/list/mp`,
		method: 'get',
		data
	})
}

// 花海世界
export function landPlant(data) {
	return request({
		url: `/jeecg-boot/land/landPlant/list/mp`,
		method: 'get',
		data
	})
}

// 通过id查询
export function landPlantQueryById(data) {
	return request({
		url: `/jeecg-boot/land/landPlant/queryById`,
		method: 'get',
		data
	})
}

// 首页通知
export function releaseNotice(data) {
	return request({
		url: `/jeecg-boot/api/park/releaseNotice`,
		method: 'get',
		data
	})
}

// 地块信息
export function landInfoList(data) {
	return request({
		url: `/jeecg-boot/land/landInfo/list/mp`,
		method: 'get',
		data
	})
}

// 扫码信息
export function getTickets(data) {
	return request({
		url: `/jeecg-boot/park/parkTicketOrderDetail/getTicket`,
		method: 'post',
		data
	})
}

// 扫码信息
export function checkTicket(data) {
	return request({
		url: `/jeecg-boot/park/parkTicketOrderDetail/checkTicket`,
		method: 'post',
		data
	})
}

// 扫码信息
export function ticketDate(data) {
	return request({
		url: `/jeecg-boot/park/parkTicketDate/record`,
		method: 'get',
		data
	})
}
