// token常量
const TokenKey = 'X-Access-Token';
const UserKey = 'X-User';

const errMsg = '出错了~';
// 获取token
export function getToken() {
	let token
	try {
		const value = uni.getStorageSync(TokenKey)
		if (value) {
			token = value
		}
	} catch (e) {
		console.log(e, errMsg)
	}
	return token
}

// 设置token
export function setToken(token) {
	try {
		uni.setStorageSync(TokenKey, token)
	} catch (e) {
		console.log(e, errMsg)
	}
}
// 清楚token
export function removeToken() {
	try {
	    uni.removeStorageSync(TokenKey)
	} catch (e) {
	    console.log(e, errMsg)
	}
}


// 获取user
export function getUser() {
	let user
	try {
		const value = uni.getStorageSync(UserKey)
		if (value) {
			user = value
		}
	} catch (e) {
		console.log(e, errMsg)
	}
	return user
}

// 设置user
export function setUser(user) {
	try {
		uni.setStorageSync(UserKey, user)
	} catch (e) {
		console.log(e, errMsg)
	}
}
// 清楚user
export function removeUser() {
	try {
	    uni.removeStorageSync(UserKey)
	} catch (e) {
	    console.log(e, errMsg)
	}
}